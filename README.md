# Kotlin MVP example #

This is an example of the MVP Pattern working with the Kotlin programming language. Take this example as a guide when using it in upcoming projects.


### What is this repository for? ###

* Kotlin example
* Model View Presenter pattern

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Paul Conroy